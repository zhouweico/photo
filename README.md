# photo
Application for viewing and querying photos.

## 产品原型

### 静态效果
![产品原型](./public/产品原型.png)

### 动态效果

![产品原型](./public/产品原型.gif)

### 原型代码

[./public/产品原型.vue](./public/产品原型.gif)。使用 [Element Plus Playground](https://element-plus.run) 在线运行。

![产品原型](./public/产品原型运行效果.png)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Multi Platform Build
```
yarn electron:build-mwl
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
